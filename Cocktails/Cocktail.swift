//
//  Cocktail.swift
//  Cocktails
//
//  Created by iem on 12/02/2015.
//  Copyright (c) 2015 iem. All rights reserved.
//

import Foundation

func == (op1 : Cocktail, op2 : Cocktail)-> Bool{
    return ((op1.name == op2.name) && (op1.ingredients == op2.ingredients) && (op1.directions == op2.directions))
}

struct Cocktail:Equatable{
    let name, ingredients, directions: String
}
