//
//  ViewController.swift
//  Cocktails
//
//  Created by iem on 12/02/2015.
//  Copyright (c) 2015 iem. All rights reserved.
//

import UIKit


class MasterViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate {
    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    
    var filteredCocktails : [Cocktail]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // chargement des données
        DataManager.sharedInstance.loadDataDromPlist()
        
        // initialisation du titre
        self.navigationItem.title = "\u{1F378} Awsome cocktails ! \u{1F379}"
        filteredCocktails = DataManager.sharedInstance.cocktails
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TableViewDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredCocktails.count
        } else {
            return DataManager.sharedInstance.cocktails.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //ask for a reusable cell from the tableview, the tableview will create a new one if it doesn't have any
        let cell = self.tableView.dequeueReusableCellWithIdentifier("CocktailCell") as UITableViewCell
        
        var cocktail : Cocktail
        // Check to see whether the normal table or search results table is being displayed and set the Candy object from the appropriate array
        if tableView == self.searchDisplayController!.searchResultsTableView {
            cocktail = filteredCocktails[indexPath.row]
        } else {
            cocktail = DataManager.sharedInstance.cocktails[indexPath.row]
        }
        
        // Configure the cell
        cell.textLabel.text = cocktail.name
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("detailView", sender: tableView)
    }
    
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete{
            
            let sourceTableView = tableView
            let sourceSearchTableView = self.searchDisplayController!.searchResultsTableView
            
            if sourceTableView == sourceSearchTableView {
                let cocktail = self.filteredCocktails[indexPath.row]
                let index = DataManager.sharedInstance.findIndexOfCocktail(cocktail)
                DataManager.sharedInstance.removeCocktailAt(index: index)
                self.filteredCocktails.removeAtIndex(indexPath.row)
                
            } else {
                
                DataManager.sharedInstance.removeCocktailAt(index: indexPath.row)
            }
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    // Mark: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "detailView" {
            let destinationViewController = segue.destinationViewController as EditCocktailViewController
            let sourceTableView = sender as UITableView
            let sourceSearchTableView = self.searchDisplayController!.searchResultsTableView
            if sourceTableView == sourceSearchTableView {
                let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!
                destinationViewController.cocktail = self.filteredCocktails[indexPath.row]
                destinationViewController.index = DataManager.sharedInstance.findIndexOfCocktail(self.filteredCocktails[indexPath.row])
            } else {
                let indexPath = self.tableView.indexPathForSelectedRow()!
                destinationViewController.cocktail = DataManager.sharedInstance.cocktails[indexPath.row]
                destinationViewController.index = indexPath.row
            }
        }
    }

    
    
    // Mark: Search
    func filterContentForSearchText(searchText: String) {
    // Filter the array using the filter method
        self.filteredCocktails = DataManager.sharedInstance.cocktails.filter({( cocktail: Cocktail) -> Bool in
            let stringMatch = cocktail.name.rangeOfString(searchText)
            return (stringMatch != nil)
        })
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.tableView.reloadData()
    }
    
}

