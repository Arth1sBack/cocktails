//
//  DataManager.swift
//  Cocktails
//
//  Created by iem on 05/03/2015.
//  Copyright (c) 2015 iem. All rights reserved.
//

import Foundation

private let _DataManagerSharedInstance = DataManager()


class DataManager {
    var cocktails: [Cocktail]!
    
    init() {
        self.cocktails = nil
    }

    // mise en place du singleton
    class var sharedInstance: DataManager {
        return _DataManagerSharedInstance
    }
    
    // chargement des données
    func loadDataDromPlist(){
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0)as NSString
        let path = documentsDirectory.stringByAppendingPathComponent("DrinkDirections.plist")
        
        let fileManager = NSFileManager.defaultManager()
        
        // Check if file exists
        if(!fileManager.fileExistsAtPath(path)){
            // If it doesn't, copy it from the default file in the Resources folder
            let bundle = NSBundle.mainBundle().pathForResource("DrinkDirections", ofType: "plist")
            fileManager.copyItemAtPath(bundle!, toPath: path, error:nil)
        }
        
        
        if let cocktailsAray = NSArray(contentsOfFile: path) as? [[String : String]] {
            cocktails = [Cocktail]()
            for dict in cocktailsAray{
                let name = dict["name"]!
                let ingredients = dict["ingredients"]!
                let directions = dict["directions"]!
                cocktails.append(Cocktail(name: name, ingredients: ingredients, directions: directions))
            }
        }
    }
    
    
    func removeCocktailAt(#index:NSInteger){
        cocktails.removeAtIndex(index)
        self.transformListIntoPlist()
    }
    
    func addCocktail(aCocktail cocktail:Cocktail){
        cocktails.append(cocktail)
        cocktails.sort({ $0.name < $1.name })
        self.transformListIntoPlist()
    }
    
    func findIndexOfCocktail(searchedCocktail:Cocktail) -> Int{
        var i = 0
        for cocktail in self.cocktails{
            if (searchedCocktail == cocktail) {
                return i
            }
            i++
        }
        return -1
    }
    
    
    func transformListIntoPlist(){
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as NSString
        let path = documentsDirectory.stringByAppendingPathComponent("DrinkDirections.plist")
        
        var dict : NSMutableArray = NSMutableArray()
        
        for (index, aCocktail)in enumerate (cocktails){
            let newCocktail = ["name":aCocktail.name ,"ingredients":aCocktail.ingredients, "directions" : aCocktail.directions]
            dict.addObject(newCocktail)
        }
        
        dict.writeToFile(path, atomically: true)
        
        let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    }
}

