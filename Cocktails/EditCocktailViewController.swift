//
//  EditCocktailViewController.swift
//  Cocktails
//
//  Created by iem on 05/03/2015.
//  Copyright (c) 2015 iem. All rights reserved.
//

import UIKit

class EditCocktailViewController: UIViewController {
    
    var cocktail: Cocktail?
    var index: NSInteger?
    
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textIngredients: UITextView!
    @IBOutlet weak var textDirection: UITextView!
    @IBOutlet weak var navBar: UINavigationItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.cocktail != nil){
            textName.text = self.cocktail?.name
            textIngredients.text = self.cocktail?.ingredients
            textDirection.text = self.cocktail?.directions
            
            self.navigationItem.title = "\((self.cocktail?.name)!) recipe"
            
            var b = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action:"editFields")
            
            self.navigationItem.rightBarButtonItem = b
            self.view.userInteractionEnabled = false
        }else{
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func editFields() {
        
        self.view.userInteractionEnabled = !self.view.userInteractionEnabled
        self.navigationItem.rightBarButtonItem?.title = (self.view.userInteractionEnabled) ? "Done" : "Edit"
        
        if (!self.view.userInteractionEnabled){
            
            DataManager.sharedInstance.removeCocktailAt(index: index!)
            let newCocktail = Cocktail(name:self.textName.text, ingredients: self.textIngredients.text, directions: self.textDirection.text)
            DataManager.sharedInstance.addCocktail(aCocktail: newCocktail)
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // bouton save
    @IBAction func endEdition(sender: AnyObject) {
        if (!self.textName.text.isEmpty && !self.textIngredients.text.isEmpty && !self.textDirection.text.isEmpty){
            let newCocktail = Cocktail(name:self.textName.text, ingredients: self.textIngredients.text, directions: self.textDirection.text)
            DataManager.sharedInstance.addCocktail(aCocktail: newCocktail)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    
    
    @IBAction func goBackToMenu(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
